const path = require("path");
const express = require("express");
const app = express();
const ejsLayouts = require('express-ejs-layouts');
const http = require("http").createServer(app);
const io = require("socket.io")(http);
const colors = require("colors");
const bodyParser = require("body-parser");
io.on('connection', function(socket){
    console.log('---- SOCKET BAĞLANTISI KURULDU ----'.cyan);
    socket.on("bildirim",()=>{
        console.log("bildirim");
        io.emit("gelen-bildirim");
    });
  });
  
//Görüntü motorumu ejs atadım
app.set("view engine", "ejs");
//view'lerimin yolunu belirttim
app.set("views", path.join(__dirname, "./views"));
//routers


//middlewares
app.use(ejsLayouts)
//static dosyalarım (css/js/vs...)
app.use("/public", express.static("./public"))
//bodyparser
app.use(bodyParser.json());
//İstek alan url'yı console a yazdıran middleware
app.use((req, res, next) => {
    console.error(req.url);
    next();
})
// panel router

require("./routers/routeManager")(app);

http.listen(81)