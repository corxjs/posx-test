module.exports.index = function(req,res){
    res.render('panel',{kurum_adi:"SOS CAFE",active:1});
}
module.exports.garsonlar = function(req,res){
    res.render('panelGarsonlar',{kurum_adi:"SOS CAFE",active:2});
}
module.exports.menu = function(req,res){
    res.render('panelMenu',{kurum_adi:"SOS CAFE",active:3});
}
module.exports.siparisler = function(req,res){
    res.render('panelSiparisler',{kurum_adi:"SOS CAFE",active:4});
}
module.exports.masalar = function(req,res){
    res.render('panelMasalar',{kurum_adi:"SOS CAFE",active:5});
}