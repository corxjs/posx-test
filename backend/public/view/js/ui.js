class UI{

   static masaEkle(eklenecek){
        let masaHtml = ` <div class="card card-stats col-lg-3">                                               
        <div>
              
                
                <span class="col-lg-12 badge badge-success">Örnek</span>
            <div class="card-header card-header-danger card-header-icon">
                <p class="card-title">Masa 1</p>
                <div class="card-icon">
                    <i class="material-icons">panorama_fish_eye</i>
                </div>


                <p class="card-category"> BOŞ </p>
                <h3 class="card-title">20 TL</h3>
            </div>
            <div class="card-footer">
                <button id="masaDurumDegistirici" class="btn btn-danger"> BOŞ </button>
                <button data-toggle="modal" data-target="#masaAyar"
                    class="btn btn-primary"> <i class="material-icons">menu</i>
                </button>
            </div>
            <!--Masa Modal Start  -->
            <div class="modal fade" id="masaAyar" tabindex="-1"
                role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"><i
                                    class="material-icons">settings_applications</i>
                                Masa 1</h5>
                            <button type="button" class="close"
                                data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <h2 class="col-6">Tutar 20 TL</h2>
                            </div>
                            <button class="col-4 col-offset-2 btn btn-success">Hesap</button>
                            <button class="col-6 btn btn-warning">Bekleyen
                                Siparişler</button>
                            <hr />
                            <button class="btn btn-rose">Etiket Ver</button>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-dark"
                                data-dismiss="modal">Kapat</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Masa Modal End  -->
        </div>
        
    </div>`;
    eklenecek.innerHTML+=masaHtml;
    
    }
    static garsonEkle(eklenecek,data){
        let garsonHtml = `    <div class="col-lg-3 col-md-3 col-sm-3">
        <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
                <div class="card-icon">
                    <i class="material-icons">person</i>
                </div>
                <p class="card-category">${data.ad}</p>
                <h3 class="card-title"> ${data.ucret}₺ </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <button class="btn btn-warning">Sil</button>

                </div>
            </div>
        </div>
</div>
            <!--Masa Modal Start  -->
            <div class="modal fade" id="masaAyar" tabindex="-1"
                role="dialog" aria-labelledby="exampleModalLabel"
                aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel"><i
                                    class="material-icons">settings_applications</i>
                                Masa 1</h5>
                            <button type="button" class="close"
                                data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <h2 class="col-6">Tutar 20 TL</h2>
                            </div>
                            <button class="col-4 col-offset-2 btn btn-success">Hesap</button>
                            <button class="col-6 btn btn-warning">Bekleyen
                                Siparişler</button>
                            <hr />
                            <button class="btn btn-rose">Etiket Ver</button>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-dark"
                                data-dismiss="modal">Kapat</button>
                        </div>
                    </div>
                </div>
            </div>
            <!--Masa Modal End  -->
        </div>
        
    </div>`;
    eklenecek.innerHTML+=garsonHtml;
    
    }
}