const panelRouter = require("./panelRouter");

module.exports = function (app) {
    app.use("/panel", panelRouter);

}
