const express = require("express");
const router = express.Router();
const ctrlPanel = require("../controller/panelController");

router.get('/',ctrlPanel.index);
router.get('/garsonlar',ctrlPanel.garsonlar);
router.get('/menu',ctrlPanel.menu);
router.get('/siparisler',ctrlPanel.siparisler);
router.get('/masalar',ctrlPanel.masalar);
module.exports = router;
