class Masa{
    constructor(no,hesap,durum,etiket,siparisler){
      this.no = no;
      this.hesap=hesap;
      this.durum=durum;
      this.etiket=etiket;
      this.siparisler=siparisler;
      if(typeof Masa.masaSayisi == 'undefined'){
        Masa.masaSayisi = 1;
      }else{
        Masa.masaArttir();
      }
     
     
    }
    getNo() {
        return this.no;
    }
    getHesap() {
        return this.hesap;
    }
    getDurum() {
        return this.durum;
    }
    getEtiket() {
        return this.etiket;
    }
    getSiparisler() {
        return this.siparisler;
    }
    getMasaSayisi(){
        return Masa.masaSayisi;
    }
    static MasaSayisi(){
        return Masa.masaSayisi;
    }
    static masaArttir(){
    Masa.masaSayisi++;
    }
    
 
}